package org.anadoxin.jenkinsrunner

import junit.framework.TestCase.*
import org.anadoxin.jenkins.BasicJenkinsClient
import org.anadoxin.jenkins.Credentials
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.net.URL

open class BasicJenkinsCommunicationTests {
    fun getResource(name: String): InputStream = this::class.java.getResourceAsStream(name)

    lateinit var config: GlobalConfig
    lateinit var creds: Credentials

    @Before
    fun setUpOne() {
        val currentDir = System.getenv("PWD")
        val configPath = File(currentDir, GlobalConfig.defaultFileName())
        val maybeConfig = GlobalConfig.fromInputStream(FileInputStream(configPath))
        assertNotNull(maybeConfig)
        if(maybeConfig != null) {
            this.config = maybeConfig
        } else {
            return
        }

        val url = URL(this.config.jenkinsURL)
        Credentials.fromNetRcHost(url).valid { v ->
            this.creds = v
        }.invalid { e ->
            assertNull(e)
        }
    }

    @Test
    fun sanityCheckForAllJenkinsTests() {
        assertTrue(true)
    }

    @Test
    fun basicJenkinsClientSanityCheck() {
        BasicJenkinsClient(URL(this.config.jenkinsURL), this.creds)
    }
}

class JenkinsCommunicationTests : BasicJenkinsCommunicationTests() {
    lateinit var client: BasicJenkinsClient

    @Before
    fun setUpOne2() {
        client = BasicJenkinsClient(URL(this.config.jenkinsURL), this.creds)
    }

    @Test
    fun liveVersionTest() {
        val versionResult = client.systemApi().jenkinsVersion()
        assertTrue(versionResult.isOk())
        assertNotNull(versionResult.ok())
        assertTrue(versionResult.ok().isNotEmpty())
    }

    @Test
    fun liveProjectListTest() {
        val projectList = client.globalApi().projectList()
        assertTrue(projectList.isOk())
        assertTrue(projectList.ok().jobs.isNotEmpty())
    }

    @Test
    fun liveQueryProjectsByName() {
        val projectList = client.globalApi().projectList()
        assertTrue(projectList.isOk())
        assertTrue(projectList.ok().jobs.isNotEmpty())

        for(proj in projectList.ok().jobs) {
            val maybeProject = client.jobApi().queryJobByName(proj.name)
            assertTrue(maybeProject.isOk())
            assertEquals(maybeProject.ok().name, proj.name)
        }
    }
}
