package org.anadoxin.jenkinsrunner

import junit.framework.TestCase.*
import org.anadoxin.jenkins.BasicJenkinsClient
import org.anadoxin.jenkins.Credentials
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.net.URL

class ConfigSanityTests {
    fun getResource(name: String): InputStream = this::class.java.getResourceAsStream(name)

    lateinit var config: GlobalConfig
    lateinit var creds: Credentials
    lateinit var client: BasicJenkinsClient

    @Before
    fun setUpOne() {
        val currentDir = System.getenv("PWD")
        val configPath = File(currentDir, GlobalConfig.defaultFileName())
        val maybeConfig = GlobalConfig.fromInputStream(FileInputStream(configPath))
        assertNotNull(maybeConfig)
        if(maybeConfig != null) {
            this.config = maybeConfig
        } else {
            return
        }

        val url = URL(this.config.jenkinsURL)
        Credentials.fromNetRcHost(url).valid { v ->
            this.creds = v
        }.invalid { e ->
            assertNull(e)
        }

        this.client = BasicJenkinsClient(URL(this.config.jenkinsURL), this.creds)
    }

    // Run this on server startup as well

    @Test
    fun validateProjectNamesInLiveJenkinsInstance() {
        assertNotEquals(this.config.projects.size, 0)

        val projectList = ArrayList<String>()
        val maybeProjects = this.client.globalApi().projectList()
        assertTrue(maybeProjects.isOk())

        for(project in maybeProjects.ok().jobs)
            projectList.add(project.name)

        assertNotEquals(projectList.size, 0)

        for((_, projectConfig) in this.config.projects) {
            assertTrue(projectList.contains(projectConfig.targetName))
        }
    }
}
