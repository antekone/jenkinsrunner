package org.anadoxin.jenkinsrunner

import junit.framework.TestCase.*
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.junit.Test
import java.io.InputStream

class GlobalConfigTests {
    fun getResource(name: String): InputStream = this::class.java.getResourceAsStream(name)

    @Test
    fun hasTestResources() {
        assertNotNull(getResource("/TestConfig1.toml"))
    }

    @Test(expected = RuntimeException::class)
    fun dontParseInvalidConfig() {
        assertNotNull(GlobalConfig.fromInputStream(getResource("/InvalidTestConfig1.toml")))
    }

    @Test(expected = RuntimeException::class)
    fun dontParseMissingConfig() {
        assertNotNull(GlobalConfig.fromInputStream(getResource("/missing")))
    }

    @Test
    fun parsesMinimalConfig() {
        val config = GlobalConfig.fromInputStream(getResource("/TestConfig1.toml"))
        assertNotNull(config)
        if(config != null) {
            assertEquals("some_url", config.jenkinsURL)
        }
    }
}