package org.anadoxin.types.results.generic

import org.anadoxin.types.Result

typealias GenericResult<T, C> = Result<T, GenericError<C>>

class GenericError<C>(
    private val code: C,
    private val errMsg: String)
{
    companion object {
        fun <C> fromString(c: C, s: String): GenericError<C> {
            return GenericError(c, s)
        }
    }

    override fun toString(): String {
        return "GenericError[code=$code, message=$errMsg]"
    }

    fun getCode() = code
    fun getErrorMessage() = errMsg
}

