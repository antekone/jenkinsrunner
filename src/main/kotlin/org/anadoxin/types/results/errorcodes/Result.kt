package org.anadoxin.types.results.errorcodes

import org.anadoxin.jenkins.ResultErrorCode
import org.anadoxin.types.results.generic.GenericResult

typealias Result<T> = GenericResult<T, ResultErrorCode>
