package org.anadoxin.types

class ResultContinuationBlock<T, F>(private val r: Result<T, F>){
    infix fun valid(block: (f: T) -> Unit): ResultContinuationBlock<T, F> {
        if(r.isOk()) {
            block(r.ok())
        }

        return this
    }

    infix fun invalid(block: (f: F) -> Unit): ResultContinuationBlock<T, F> {
        if(r.isErr()) {
            block(r.err())
        }

        return this
    }
}

class Result<T, F> {
    private var t: T? = null
    private var f: F? = null

    fun ok(): T = this.t!!
    fun err(): F = this.f!!
    fun getOrNull(): T? = this.t

    operator fun component1(): T? = t
    operator fun component2(): F? = f

    fun isOk(): Boolean = this.t != null
    fun isErr(): Boolean = this.f != null
    fun check(): ResultContinuationBlock<T, F> = ResultContinuationBlock(this)

    infix fun valid(block: (t: T) -> Unit): ResultContinuationBlock<T, F> {
        val c = ResultContinuationBlock(this)
        c.valid(block)
        return c
    }

    infix fun invalid(block: (f: F) -> Unit): ResultContinuationBlock<T, F> {
        val c = ResultContinuationBlock(this)
        c.invalid(block)
        return c
    }

    override fun toString(): String {
        return if(isOk()) {
            "GenericResult[OK: ${this.t}]"
        } else {
            "GenericResult[ERR: ${this.f}]"
        }
    }

    companion object {
        fun <T, F> ok(value: T): Result<T, F> {
            val result = Result<T, F>()
            result.t = value
            return result
        }

        fun <T, F> err(value: F): Result<T, F> {
            val result = Result<T, F>()
            result.f = value
            return result
        }
    }
}
