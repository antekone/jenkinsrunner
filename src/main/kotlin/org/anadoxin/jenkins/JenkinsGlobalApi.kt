package org.anadoxin.jenkins

import org.anadoxin.jenkins.dao.ProjectListDAO

class JenkinsGlobalApi(private val client: BasicJenkinsClient, private val cache: GlobalApiCache) {
    fun projectList(): APIResult<ProjectListDAO> {
        if(cache.projectListCache == null) {
            val reply = client.requestObject("api/json", ProjectListDAO.Deserializer())
            if(reply.isErr())
                return APIResult.err(reply.err())

            val (lst, _) = reply.ok()
            cache.projectListCache = lst
        }

        return makeOkAPIResult(cache.projectListCache!!)
    }
}
