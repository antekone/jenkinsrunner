package org.anadoxin.jenkins

import org.anadoxin.types.results.generic.GenericError
import org.anadoxin.types.results.generic.GenericResult

enum class APIErrorCode {
    OK,
    ConnectionFailed,
    InvalidHTTPResponse,
    InvalidJSONResponse,
    NotImplemented,
    RequestInterrupted,
}

typealias APIResult<T> = GenericResult<T, APIErrorCode>

fun <T> makeErrAPIResult(code: APIErrorCode, msg: String): APIResult<T> =
    APIResult.err(GenericError.fromString(code, msg))

fun <T> makeOkAPIResult(t: T): APIResult<T> =
    APIResult.ok(t)
