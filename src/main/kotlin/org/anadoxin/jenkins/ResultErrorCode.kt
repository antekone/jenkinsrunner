package org.anadoxin.jenkins

import org.anadoxin.types.results.generic.GenericError
import org.anadoxin.types.results.errorcodes.Result

enum class ResultErrorCode {
    OK,
    CredentialsNotFound
}

fun <T> genericOkResult(t: T): Result<T> {
    return Result.ok(t)
}

fun <T> genericErrorResult(code: ResultErrorCode, s: String): Result<T> {
    return Result.err(GenericError.fromString(code, s))
}
