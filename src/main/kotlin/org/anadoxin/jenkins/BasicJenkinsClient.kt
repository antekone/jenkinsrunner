package org.anadoxin.jenkins

import com.github.kittinunf.fuel.core.ResponseDeserializable
import org.anadoxin.http.BodyWithHeaders
import org.anadoxin.http.ObjectWithHeaders
import org.anadoxin.http.RestClient
import org.anadoxin.jenkins.dao.ProjectListDAO
import java.net.URL

interface JenkinsClient {
    fun systemApi(): JenkinsSystemApi
    fun globalApi(): JenkinsGlobalApi
    fun jobApi(): JenkinsJobApi

    fun requestGet(path: String): APIResult<BodyWithHeaders>
    fun requestRawGet(url: String): APIResult<BodyWithHeaders>
    fun requestRawPostJSON(url: String, payloadJSON: String): APIResult<BodyWithHeaders>
}

class GlobalApiCache {
    var projectListCache: ProjectListDAO? = null
}

class BasicJenkinsClient(private val url: URL, creds: Credentials) : JenkinsClient {
    val restClient = RestClient(creds)
    var crumb: String? = null
    val globalApiCache = GlobalApiCache()

    override fun systemApi(): JenkinsSystemApi {
        return JenkinsSystemApi(this)
    }

    override fun globalApi(): JenkinsGlobalApi {
        return JenkinsGlobalApi(this, globalApiCache)
    }

    override fun jobApi(): JenkinsJobApi {
        return JenkinsJobApi(this)
    }

    override fun requestGet(path: String): APIResult<BodyWithHeaders> {
        return restClient.requestGet("$url/$path")
    }

    override fun requestRawGet(url: String): APIResult<BodyWithHeaders> {
        return restClient.requestRawGet(url)
    }

    override fun requestRawPostJSON(url: String, payload: String): APIResult<BodyWithHeaders> {
        return restClient.requestRawPostJSON(url, payload)
    }

    fun <T : Any> requestObject(path: String, deserializer: ResponseDeserializable<T>): APIResult<ObjectWithHeaders<T>> {
        return restClient.requestGetObject("$url/$path", deserializer)
    }

    fun requestJson(json: String): String {
        if(crumb == null) {
            requestCrumb()
        }

        return ""
    }

    fun requestCrumb() {
        // ...
    }
}
