package org.anadoxin.jenkins

import co.cdjones.security.auth.NetrcParser
import org.anadoxin.types.results.errorcodes.Result
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class Credentials private constructor() {
    var username: String? = null
    var password: String? = null
    var bearerToken: String? = null

    companion object {
        fun create(username: String, password: String): Credentials {
            val c = Credentials()
            c.username = username
            c.password = password
            return c
        }

        fun create(token: String): Credentials {
            val c = Credentials()
            c.bearerToken = token
            return c
        }

        fun fromNetRcHost(url: URL): Result<Credentials> {
            val rawCreds = NetrcParser.getInstance().getCredentials(url.host)

            return if(rawCreds != null) {
                genericOkResult(this.create(rawCreds.user(), rawCreds.password()))
            } else {
                genericErrorResult(ResultErrorCode.CredentialsNotFound,
                        "Can't locate credentials for host ${url.host} in .netrc file")
            }
        }

        fun fromNetRcHost(hostname: String): Result<Credentials> {
            return try {
                return this.fromNetRcHost(URL(hostname))
            } catch(url: MalformedURLException) {
                genericErrorResult(ResultErrorCode.CredentialsNotFound,
                    "Can't extract hostname: " + url.message!!)
            }
        }

        fun fromBearerToken(token: String): Result<Credentials> {
            return Result.ok(this.create(token))
        }
    }

    fun base64(): String {
        val sb = StringBuffer()

        sb.append(this.username)
        sb.append(":")
        sb.append(this.password)

        return Base64.getEncoder().encodeToString(sb.toString().toByteArray())
    }

    fun getToken(): String = this.bearerToken!!
    fun hasToken(): Boolean = this.bearerToken != null
    fun hasUsernameAndPassword(): Boolean = this.username != null && this.password != null
}
