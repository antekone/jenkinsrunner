package org.anadoxin.jenkins.dao

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class FreeStyleBuild(
    val number: Int,
    val url: String
)

data class HealthReportItem(
    val description: String,
    val iconClassName: String,
    val iconUrl: String,
    val score: Int
)

data class FreeStyleProject(
    val description: String,
    val displayName: String,
    val fullName: String,
    val name: String,
    val url: String,
    val buildable: Boolean,
    val builds: List<FreeStyleBuild>,
    val color: String,
    val firstBuild: FreeStyleBuild?,
    val healthReport: List<HealthReportItem>,
    val inQueue: Boolean,
    val keepDependencies: Boolean,
    val lastBuild: FreeStyleBuild?,
    val lastCompletedBuild: FreeStyleBuild?,
    val lastFailedBuild: FreeStyleBuild?,
    val lastStableBuild: FreeStyleBuild?,
    val lastSuccessfulBuild: FreeStyleBuild?,
    val lastUnstableBuild: FreeStyleBuild?,
    val lastUnsuccessfulBuild: FreeStyleBuild?,
    val nextBuildNumber: Int,
    val concurrentBuild: Boolean,
    val labelExpression: String
) {
    class Deserializer : ResponseDeserializable<FreeStyleProject> {
        override fun deserialize(content: String) = Gson().fromJson(content, FreeStyleProject::class.java)!!
    }
}
