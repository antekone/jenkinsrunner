package org.anadoxin.jenkins.dao

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class Job(
    val name: String,
    val url: String
)

data class ProjectListDAO(
    val jobs: List<Job>
) {
    class Deserializer : ResponseDeserializable<ProjectListDAO> {
        override fun deserialize(content: String) = Gson().fromJson(content, ProjectListDAO::class.java)!!
    }

    fun getJobByName(n: String): Job? {
        val filtered = jobs.filter { j -> j.name == n }.toList()
        return if(filtered.isEmpty()) {
            null
        } else {
            filtered.first()
        }
    }
}
