package org.anadoxin.jenkins

import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.net.URL

class CredentialsBuilder {
    enum class AuthConfigType {
        None, Basic, Bearer
    }

    private var type: AuthConfigType = AuthConfigType.None
    private var source: String? = null
    private var basic: Boolean = false
    private var bearer: Boolean = false
    private var host: URL? = null

    companion object {
        fun empty(): CredentialsBuilder {
            return CredentialsBuilder()
        }

        fun forType(type: AuthConfigType): CredentialsBuilder {
            val b = CredentialsBuilder()
            return b.withType(type)
        }
    }

    fun withType(t: AuthConfigType): CredentialsBuilder {
        this.type = t
        return this
    }

    fun withBasicSource(s: String): CredentialsBuilder {
        this.source = s
        this.basic = true
        return this
    }

    fun withBearerSource(s: String): CredentialsBuilder {
        this.source = s
        this.bearer = true
        return this
    }

    fun withUrl(h: URL): CredentialsBuilder {
        this.host = h
        return this
    }

    fun create(): Credentials? {
        return if(this.basic) {
            createBasic()
        } else if(this.bearer) {
            createBearer()
        } else {
            null
        }
    }

    private fun createBasic(): Credentials? {
        if(this.source == "netrc") {
            if(this.host == null)
                return null

            val ret = Credentials.fromNetRcHost(this.host!!)
            return if(ret.isOk())
                ret.ok()
            else
                null
        } else {
            return null
        }
    }

    private fun createBearer(): Credentials? {
        if(this.source == null)
            return null

        return if(this.source!!.startsWith("file:")) {
            val fileName = this.source!!.replaceFirst("file:", "")
            val token = BufferedReader(InputStreamReader(FileInputStream(File(fileName)))).use { it.readText() }.trim {
                it == ' ' || it == '\t' || it == '\n' || it == '\r'
            }

            Credentials.fromBearerToken(token).ok()
        } else {
            null
        }
    }
}
