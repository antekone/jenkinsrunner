package org.anadoxin.jenkins

import org.anadoxin.jenkins.dao.FreeStyleProject

class JenkinsJobApi(private val client: BasicJenkinsClient) {
    fun queryJobByName(name: String): APIResult<FreeStyleProject> {
        val reply = client.requestObject("job/$name/api/json", FreeStyleProject.Deserializer())
        if(reply.isErr())
            return APIResult.err(reply.err())

        val (p, _) = reply.ok()
        return makeOkAPIResult(p)
    }
}
