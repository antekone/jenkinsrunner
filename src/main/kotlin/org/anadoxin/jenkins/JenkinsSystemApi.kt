package org.anadoxin.jenkins

class JenkinsSystemApi(private val client: BasicJenkinsClient) {
    fun jenkinsVersion(): APIResult<String> {
        val resp = client.requestGet("api/json")
        if(resp.isErr())
            return APIResult.err(resp.err())

        val r = resp.ok()
        return try {
            makeOkAPIResult(r.headers["X-Jenkins"].first())
        } catch(e: NoSuchElementException) {
            makeErrAPIResult(APIErrorCode.InvalidHTTPResponse, "Missing X-Jenkins header in response")
        }
    }

    fun hudsonVersion(): APIResult<String> {
        val resp = client.requestGet("api/json")
        if(resp.isErr())
            return APIResult.err(resp.err())

        val r = resp.ok()
        return try {
            makeOkAPIResult(r.headers["X-Hudson"].first())
        } catch(e: NoSuchElementException) {
            makeErrAPIResult(APIErrorCode.InvalidHTTPResponse, "Missing X-Hudson header in response")
        }
    }
}
