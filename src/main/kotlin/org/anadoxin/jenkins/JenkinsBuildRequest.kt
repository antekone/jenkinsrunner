package org.anadoxin.jenkins

import io.mikael.urlbuilder.UrlBuilder
import io.mikael.urlbuilder.util.RuntimeMalformedURLException
import org.anadoxin.jenkins.dao.Job
import org.anadoxin.jenkinsrunner.common.BuildObject
import org.anadoxin.jenkinsrunner.common.BuildObjects
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.anadoxin.types.Result
import org.slf4j.LoggerFactory
import java.net.URL

class JenkinsBuildRequest(
    private val client: JenkinsClient,
    private val config: GlobalConfig)
{
    private val logger = LoggerFactory.getLogger(this::class.java)

    private fun buildWithParams(
        buildObject: BuildObject,
        configProject: GlobalConfig.ConfigProject,
        jenkinsJobName: Job
    ): URL {
        // assume jenkinsJobName == configProject.targetName
        var builder = createBuilder(configProject, jenkinsJobName)

        configProject.targetArgs.forEach { (k, v) ->
            if(v == "\$GitCommit") {
                builder = builder.addParameter(k, buildObject.gitHash)
            } else if(v == "\$RefId") {
                builder = builder.addParameter(k, buildObject.refId)
            } else {
                throw RuntimeException("Unsupported URL option: $v")
            }
        }

        return builderToURL(builder)
    }

    private fun buildWithoutParams(
        configProject: GlobalConfig.ConfigProject,
        jenkinsJobName: Job
    ): URL {
        val builder = createBuilder(configProject, jenkinsJobName)
        return builderToURL(builder)
    }

    private fun builderToURL(builder: UrlBuilder): URL {
        try {
            val resultingUrl = builder.toUrl()
            if(resultingUrl == null) {
                throw RuntimeException("URL build failure")
            } else {
                return resultingUrl
            }
        } catch(e: RuntimeMalformedURLException) {
            throw RuntimeException("URL build failure")
        }
    }

    private fun createBuilder(
        configProject: GlobalConfig.ConfigProject,
        jenkinsJobName: Job
    ): UrlBuilder {
        var builder = UrlBuilder.fromString("${config.jenkinsURL}/job/${jenkinsJobName.name}/buildWithParameters")
        if(configProject.token != null)
            builder = builder.addParameter("token", configProject.token)

        return builder
    }

    fun buildRequestURL(buildObject: BuildObject): URL {
        val plist = client.globalApi().projectList().getOrNull() ?:
            throw RuntimeException("Jenkins communication error")

        val configProject = config.getConfigProject(buildObject.fullProjectName) ?:
            throw RuntimeException("No such job: ${buildObject.fullProjectName}")

        val jenkinsJobName = plist.getJobByName(configProject.targetName) ?:
            throw RuntimeException("Configuration is not synchronized!")

        return if(configProject.targetArgs.isEmpty()) {
            buildWithoutParams(configProject, jenkinsJobName)
        } else {
            buildWithParams(buildObject, configProject, jenkinsJobName)
        }
    }
}
