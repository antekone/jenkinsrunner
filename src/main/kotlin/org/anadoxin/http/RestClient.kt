package org.anadoxin.http

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.core.extensions.jsonBody
import org.anadoxin.jenkins.APIErrorCode
import org.anadoxin.jenkins.APIResult
import org.anadoxin.jenkins.Credentials
import org.anadoxin.types.results.generic.GenericError
import java.net.URL

fun <T : APIResult<out Any>> retry(howManyTimes: Int, func: () -> T): T {
    var ret: T? = null

    for(i in 1..howManyTimes) {
        ret = func()
        if(ret.isOk())
            return ret
    }

    return ret!!
}

data class BodyWithHeaders(val body: String, val headers: Headers)
data class ObjectWithHeaders<T>(val obj: T, val headers: Headers)

class RestClient(val creds: Credentials) {
    companion object {
        private const val RETRY_COUNT = 3
    }

    private fun fuelErrorToAPIErrorCode(e: FuelError): GenericError<APIErrorCode> {
        if(e.causedByInterruption)
            return GenericError.fromString(APIErrorCode.RequestInterrupted, "interrupted by user")

        return if(e.response.statusCode in (200..399)) {
            GenericError.fromString(APIErrorCode.OK, "this shouldn't happen")
        } else {
            val string = when {
                e.response.responseMessage.isNotEmpty() -> e.response.responseMessage
                (e.exception.message ?: "").isNotEmpty() -> e.exception.message!!
                else -> "unknown reason"
            }

            GenericError.fromString(APIErrorCode.InvalidHTTPResponse, string)
        }
    }

    private fun fuelRequest(path: String, post: Boolean): Request {
        val fuel = if(post) { Fuel.post(path) } else { Fuel.get(path) }
        val req = fuel.authentication()
        return when {
            creds.hasUsernameAndPassword() -> req.basic(creds.username!!, creds.password!!)
            creds.hasToken() -> req.bearer(creds.getToken())
            else -> throw RuntimeException("Invalid Fuel request")
        }
    }

    private fun fuelGetRequest(path: String): Request = this.fuelRequest(path, false)
    private fun fuelPostRequest(path: String): Request = this.fuelRequest(path, true)

    // Merge all request* methods into one

    fun requestRawPostJSON(url: String, payload: String): APIResult<BodyWithHeaders> {
        return retry(RETRY_COUNT) {
            var ret: APIResult<BodyWithHeaders>? = null

            fuelPostRequest(url).jsonBody(payload).responseString { _, resp, res ->
                res.fold(success = { buf: String ->
                    ret = APIResult.ok(BodyWithHeaders(buf, resp.headers))
                }, failure = { err: FuelError ->
                    ret = APIResult.err(fuelErrorToAPIErrorCode(err))
                })
            }.join()

            ret!!
        }
    }

    fun requestRawGet(url: String): APIResult<BodyWithHeaders> {
        return retry(RETRY_COUNT) {
            var ret: APIResult<BodyWithHeaders>? = null

            fuelGetRequest(url).responseString { _, resp, res ->
                res.fold(success = { buf: String ->
                    ret = APIResult.ok(BodyWithHeaders(buf, resp.headers))
                }, failure = { err: FuelError ->
                    ret = APIResult.err(fuelErrorToAPIErrorCode(err))
                })
            }.join()

            ret!!
        }
    }

    fun requestGet(path: String): APIResult<BodyWithHeaders> {
        return retry(RETRY_COUNT) {
            var ret: APIResult<BodyWithHeaders>? = null

            fuelGetRequest(path).responseString { _, resp, res ->
                res.fold(success = { buf: String ->
                    ret = APIResult.ok(BodyWithHeaders(buf, resp.headers))
                }, failure = { err: FuelError ->
                    ret = APIResult.err(fuelErrorToAPIErrorCode(err))
                })
            }.join()

            ret!!
        }
    }

    fun <T : Any> requestGetObject(path: String, deserializer: ResponseDeserializable<T>): APIResult<ObjectWithHeaders<T>> {
        return retry(RETRY_COUNT) {
            var ret: APIResult<ObjectWithHeaders<T>>? = null

            fuelGetRequest(path).responseObject(deserializer) { _, resp, res ->
                res.fold(success = { obj: T ->
                    ret = APIResult.ok(ObjectWithHeaders(obj, resp.headers))
                }, failure = { err: FuelError ->
                    ret = APIResult.err(fuelErrorToAPIErrorCode(err))
                })
            }.join()

            ret!!
        }
    }
}
