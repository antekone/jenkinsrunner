package org.anadoxin.jenkinsrunner.request

data class GitHubRequest(
    val ref: String,
    val after: String,
    val head_commit: HeadCommit,
    val repository: Repository
) {
    data class HeadCommit(
        val id: String
    )

    data class Owner(
        val id: Long,
        val name: String,
        val login: String
    )

    data class Repository(
        val full_name: String,
        val id: Long,
        val name: String,
        val owner: Owner
    )
}
