package org.anadoxin.jenkinsrunner.request

data class UserInfo(
    val uuid: String
)

data class Repository(
    val name: String,
    val full_name: String,
    val type: String,
    val uuid: String,
    val owner: UserInfo
)

data class CommitDescriptor(
    val hash: String
)

data class RefInfo(
    val name: String,
    val type: String   // "branch"
)

data class ChangeDescriptor(
    val commits: List<CommitDescriptor>,

    // When a branch is deleted, 'new' is null
    val new: RefInfo?,

    // When a branch is created, 'old' is null
    val old: RefInfo?
)

data class PushEvent(
    val changes: List<ChangeDescriptor>
)

data class BitBucketCloudRequest(
    val repository: Repository,
    val push: PushEvent?
)
