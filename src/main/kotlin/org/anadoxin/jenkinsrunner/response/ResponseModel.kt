package org.anadoxin.jenkinsrunner.response

import com.google.gson.Gson
import org.springframework.http.ResponseEntity

data class ErrorResponse(
    val status: Int,
    val error: String,
    val message: String
) {
    companion object {
        fun respond(status: Int, error: String, message: String): ResponseEntity<String> {
            return ResponseEntity.status(500).body(Gson().toJson(ErrorResponse(status, error, message)))
        }
    }
}

data class NotFoundResponse(
    val status: Int,
    val error: String,
    val message: String
) {
    companion object {
        fun respond(status: Int, error: String, message: String): ResponseEntity<String> {
            return ResponseEntity.status(404).body(Gson().toJson(ErrorResponse(status, error, message)))
        }
    }
}

data class OKResponse(
    val status: Int,
    val message: String
) {
    companion object {
        fun respond(message: String): ResponseEntity<String> {
            return ResponseEntity.status(200).body(Gson().toJson(OKResponse(0, message)))
        }
    }
}

data class ProjectListResponse(
    val status: Int,
    val projects: List<String>
) {
    companion object {
        fun respond(lst: List<String>): ResponseEntity<String> {
            return ResponseEntity.ok(Gson().toJson(ProjectListResponse(0, lst)))
        }
    }
}

data class ProjectInfoResponse(
    val status: Int,
    val sourceName: String,
    var targetName: String
) {
    companion object {
        fun respond(srcName: String, targetName: String): ResponseEntity<String> {
            return ResponseEntity.ok(Gson().toJson(ProjectInfoResponse(0, srcName, targetName)))
        }
    }
}
