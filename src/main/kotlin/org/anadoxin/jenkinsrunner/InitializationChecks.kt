package org.anadoxin.jenkinsrunner

import org.anadoxin.jenkins.JenkinsClient
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.slf4j.LoggerFactory

class InitializationChecks {
    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)

        fun checkProjectNameConsistency(client: JenkinsClient, config: GlobalConfig): Boolean {
            logger.info("Performing initial query to Jenkins to get project list...")

            val projectList = client.globalApi().projectList()
            if(projectList.isErr()) {
                logger.error("It's impossible to query Jenkins to read job list. Check your Jenkins configuration!")
                return false
            }

            val lst = ArrayList<String>()
            for(project in projectList.ok().jobs)
                lst.add(project.name)

            logger.info("Jenkins query successfull, cross-checking with projects from your configuration...")

            var failure = false
            config.projects.forEach { (_, projectConfig) ->
                if(!lst.contains(projectConfig.targetName)) {
                    logger.error("Configuration contains project ${projectConfig.targetName} which doesn't exist in Jenkins!")
                    failure = true
                }
            }

            if(failure) {
                logger.error("Initialization sanity checks failed. Please fix the problems listed above and retry.")
            } else {
                logger.info("Initialization sanity checks passed OK.")
            }

            return !failure
        }
    }
}

