package org.anadoxin.jenkinsrunner.state

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import org.anadoxin.http.RestClient
import org.anadoxin.jenkins.JenkinsBuildRequest
import org.anadoxin.jenkins.JenkinsClient
import org.anadoxin.jenkinsrunner.common.BuildObjectProcessor
import org.anadoxin.jenkinsrunner.processors.bitbucketcloud.BitBucketCloudProcessor
import org.anadoxin.jenkinsrunner.common.ResponseBuilderFactory
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.anadoxin.jenkinsrunner.db.Database
import org.anadoxin.jenkinsrunner.db.dao.*
import org.anadoxin.jenkinsrunner.processors.github.GitHubProcessor
import org.anadoxin.jenkinsrunner.request.BitBucketCloudRequest
import org.anadoxin.jenkinsrunner.request.GitHubRequest
import org.joda.time.Hours
import org.joda.time.Seconds
import org.slf4j.LoggerFactory

class StateMonitor : Thread() {
    data class QueueResponseExecutable(val number: Int, val url: String)
    data class QueueResponseTask(val name: String)
    data class QueueResponse(val id: Int, val task: QueueResponseTask, val executable: QueueResponseExecutable?)
    data class JobResponse(val number: Int, val queueId: Int, val result: String?, val url: String, val building: Boolean)

    private val log = LoggerFactory.getLogger(StateMonitor::class.java)!!
    var fullyInitialized = false

    lateinit var client: JenkinsClient
    lateinit var config: GlobalConfig

    companion object {
        var singleton: StateMonitor? = null

        const val TICK_TIME: Long = 2000

        fun instance(): StateMonitor {
            if(singleton == null) {
                val s = StateMonitor()
                s.start()
                singleton = s
            }

            return singleton!!
        }
    }

    fun setJenkinsClient(client: JenkinsClient) {
        this.client = client
        updateInitializationFlag()
    }

    fun setGlobalConfig(config: GlobalConfig) {
        this.config = config
        updateInitializationFlag()
    }

    private fun updateInitializationFlag() {
        this.fullyInitialized = ::client.isInitialized && ::config.isInitialized
    }

    override fun run() {
        log.info("Starting state monitor thread")

        try {
            while(true) {
                Thread.sleep(TICK_TIME)
                if(!fullyInitialized)
                    continue

                // Based on requests, create a queue
                runRequest()

                // For every queue, check if it has a job
                runQueueMonitor()

                // For every job, check its status, create notifications if necessary
                runJobMonitor()

                // For every notification, send it to the proper endpoint, based on
                // a particular request id
                runNotificationMonitor()
            }
        } catch (_: InterruptedException) {
            // Quit thread
        }
    }

    private fun runRequest() {
        val requestDao = Database().createDao<CIRequest>()
        val query = requestDao.queryBuilder()

        query
            .orderBy(CIRequest.UPDATED, true)
            .limit(1)
            .where()
            .eq(CIRequest.HANDLED, 0)

        requestDao.query(query.prepare()).forEach { r ->
            processRequest(r)
        }
    }

    private fun runQueueMonitor() {
        val queueDao = Database().createDao<CIQueue>()
        val query = queueDao.queryBuilder()

        query.orderBy(CIQueue.UPDATED, true)
            .limit(1)
            .where()
            .eq(CIQueue.HANDLED, 0)

        queueDao.query(query.prepare()).forEach { q ->
            processQueue(q)
        }
    }

    private fun queueByJob(job: CIJob): CIQueue? {
        val queueDao = Database().createDao<CIQueue>()
        val query = queueDao.queryBuilder()

        query.limit(1).where().eq(CIQueue.QUEUE_ID, job.queueId)
        queueDao.query(query.prepare()).forEach { q -> return q }
        return null
    }

    private fun requestByQueue(queue: CIQueue): CIRequest? {
        val requestDao = Database().createDao<CIRequest>()
        val query = requestDao.queryBuilder()

        query.limit(1).where().eq(CIRequest.ID, queue.requestId)
        requestDao.query(query.prepare()).forEach { r -> return r }
        return null
    }

    private fun jobByQueue(queue: CIQueue): CIJob? {
        val jobDao = Database().createDao<CIJob>()
        val query = jobDao.queryBuilder()

        query.limit(1).where().eq(CIJob.QUEUE_ID, queue.id)
        jobDao.query(query.prepare()).forEach { r -> return r }
        return null
    }

    private fun runJobMonitor() {
        val jobDao = Database().createDao<CIJob>()
        val query = jobDao.queryBuilder()

        query.orderBy(CIJob.UPDATED, true)
            .limit(1)

        jobDao.query(query.prepare()).forEach { j ->
            processJob(j)
        }
    }

    private fun runNotificationMonitor() {
        val notificationDao = Database().createDao<CINotification>()
        val query = notificationDao.queryBuilder()

        query.orderBy(CINotification.UPDATED, true)
            .limit(3)
            .where()
            .eq(CINotification.FAILED, 0)
            .and()
            .eq(CINotification.SENT, 0)

        notificationDao.query(query.prepare()).forEach { n ->
            processNotification(n)
        }
    }

    private fun queueByRemoteId(queueRemoteId: Int): CIQueue? {
        val dao = Database().createDao<CIQueue>()
        val query = dao.queryBuilder()

        query.limit(1).where().eq(CIQueue.REMOTE_QUEUE_ID, queueRemoteId)
        dao.query(query.prepare()).forEach { q -> return q }
        return null
    }

    private fun queueById(id: Int): CIQueue? {
        val dao = Database().createDao<CIQueue>()
        val query = dao.queryBuilder()

        query.limit(1).where().eq(CIQueue.QUEUE_ID, id)
        dao.query(query.prepare()).forEach { q -> return q }
        return null
    }

    private inline fun <reified T> updateObject(obj: T) = Database().createDao<T>().update(obj)
    private inline fun <reified T> createOrUpdateObject(obj: T) = Database().createDao<T>().createOrUpdate(obj)

    private fun processRequest(ciRequest: CIRequest) {
        val r = ciRequest.getRequest()
        if(r.body == null) {
            log.error("Error: NULL body encountered, failing this request")
            ciRequest.failed = true
            ciRequest.updateTime()
            this.updateObject(ciRequest)
            return
        }

        val success = when(r.engine) {
            RequestEngine.BitBucketCloud -> processAbstractRequest<BitBucketCloudRequest>(ciRequest, r.body) { rq ->
                BitBucketCloudProcessor(rq)
            }

            RequestEngine.GitHub -> processAbstractRequest<GitHubRequest>(ciRequest, r.body) { rq ->
                GitHubProcessor(rq)
            }

            else -> {
                log.error("Error: unknown engine found in this request")
                false
            }
        }

        ciRequest.failed = !success
        ciRequest.handled = true
        ciRequest.updateTime()
        this.updateObject(ciRequest)
    }

    private fun processQueue(ciQueue: CIQueue) {
        val url = ciQueue.requestURL
        val response = client.requestRawGet("$url/api/json")
        if(response.isErr()) {
            ciQueue.handled = true
            ciQueue.updateTime()
            this.updateObject(ciQueue)
            log.error("Error when trying to read queue's request url")
            return
        }

        val responseObject = Gson().fromJson(response.ok().body, QueueResponse::class.java)
        if(responseObject == null) {
            ciQueue.handled = true
            ciQueue.updateTime()
            this.updateObject(ciQueue)
            log.error("Error when reading JSON of queue")
            return
        }

        if(responseObject.executable == null) {
            ciQueue.updateTime()

            val queueAge = Math.abs(Seconds.secondsBetween(ciQueue.created, ciQueue.updated).seconds)
            val maxAge = Hours.hours(4).toStandardSeconds().seconds

            if(queueAge > maxAge) {
                ciQueue.handled = true
                log.error("Timeout error: queue didn't allocate a new job, dropping this queue from monitoring")
            } else {
                // log.info("Queue doesn't have a job yet, queue age: $queueAge seconds, max age is $maxAge seconds")
                // log.info("queue: $responseObject")
            }

            this.updateObject(ciQueue)
            return
        }

        ciQueue.handled = true
        ciQueue.updateTime()
        this.updateObject(ciQueue)

        log.info("Queue ${ciQueue.id} has allocated a new job ${responseObject.task.name}/${responseObject.executable.number}")
        val ciJob = CIJob.create(ciQueue.id, responseObject.task.name, responseObject.executable.url)
        this.createOrUpdateObject(ciJob)
    }

    private inline fun <reified R> processAbstractRequest(
        ciRequest: CIRequest,
        jsonString: String, processorBuilder: (R) -> (BuildObjectProcessor)
    ): Boolean {
        val req = Gson().fromJson(jsonString, R::class.java)
        val maybeBuilds = processorBuilder(req).extract()
        if(maybeBuilds.isErr()) {
            // This shouldn't happen, because this request should have been already validated by the very same
            // build object processor as now.
            log.error("Invalid json/no builds, failing request")
            return false
        }

        var someFailures = false

        maybeBuilds.ok().forEach { build ->
            val builder = JenkinsBuildRequest(client, config)
            val url = builder.buildRequestURL(build)
            // log.info("Will enter URL: $url")

            val maybeResponse = client.requestRawGet(url.toString())
            if(maybeResponse.isErr()) {
                log.error("API error when trying to request build via build URL")
                someFailures = true
                return@forEach
            }

            val response = maybeResponse.ok()
            val location = response.headers["Location"]
            if(location.isEmpty()) {
                log.error("Error: Jenkins has not returned a Location header after invoking build URL")
                someFailures = true
                return@forEach
            }

            val locationUrl = location.first()
            val maybeQueueJsonString = this.client.requestRawGet("$locationUrl/api/json")
            if(maybeQueueJsonString.isErr()) {
                log.error("Error: Can't read queue details, skipping this queue. HTTP error: ${maybeQueueJsonString.err()}")
                someFailures = true
                return@forEach
            }

            try {
                val queueObject = Gson().fromJson(maybeQueueJsonString.ok().body, QueueResponse::class.java)
                if(queueObject == null) {
                    log.error("Error: Queue object can't be deserialized from json string: ${maybeQueueJsonString.ok().body}")
                    someFailures = true
                    return@forEach
                }

                if(this.queueByRemoteId(queueObject.id) != null) {
                    log.error("Duplicate remote queue id detected: ${queueObject.id}, skipping this request entirely.")
                    someFailures = true
                    return@forEach
                }

                val queue = CIQueue.create(queueObject.id, ciRequest.id, build, locationUrl)
                if(0 == this.createOrUpdateObject(queue).numLinesChanged) {
                    log.error("Error when trying to persist queue: ${queue.requestURL}")
                    someFailures = true
                    return@forEach
                }

                this.createNotification(queue, CIJobResult.Created)
            } catch(e: JsonSyntaxException) {
                log.error("Error: JsonSyntaxException in response: ${maybeQueueJsonString.ok().body}")
                someFailures = true
                return@forEach
            }
        }

        return !someFailures
    }

    private fun requestJob(ciJob: CIJob): JobResponse? {
        val maybeJobResponse = this.client.requestRawGet("${ciJob.url}/api/json")
        if(maybeJobResponse.isErr()) {
            ciJob.result = CIJobResult.Failure
            ciJob.updateTime()
            this.updateObject(ciJob)

            log.error("HTTP error when querying for the job for queue ${ciJob.queueId}")
            return null
        }

        val jobResponse = Gson().fromJson(maybeJobResponse.ok().body, JobResponse::class.java)
        if(jobResponse == null) {
            ciJob.result = CIJobResult.Failure
            ciJob.updateTime()
            this.updateObject(ciJob)

            log.error("JSON error when querying for the job for queue ${ciJob.queueId}. JSON was: ${maybeJobResponse.ok().body}")
            return null
        }

        return jobResponse
    }

    private fun processJob(ciJob: CIJob) {
        val jobResponse = this.requestJob(ciJob) ?: return
        val oldResult = ciJob.result

        val ciQueue = this.queueByJob(ciJob)
        if(ciQueue == null) {
            log.error("Database corrupt; can't locate queue from job id ${ciJob.id}; skipping notification!")
            return
        }

        if(ciJob.result == CIJobResult.Created) {
            if(jobResponse.building) {
                ciJob.result = CIJobResult.Started
            }
        }

        if(jobResponse.result != null) {
            when(jobResponse.result) {
                "SUCCESS" -> ciJob.result = CIJobResult.Success
                "FAILURE" -> ciJob.result = CIJobResult.Failure
                else -> ciJob.result = CIJobResult.Failure
            }
        }

        if(ciJob.result != oldResult) {
            this.createNotification(ciQueue, ciJob.result)
        }

        ciJob.updateTime()
        this.updateObject(ciJob)
    }

    private fun createNotification(ciQueue: CIQueue, newResult: CIJobResult) {
        val notification = CINotification.create(ciQueue.id, newResult)
        createOrUpdateObject(notification)
        log.info("Queue ${ciQueue.id} new status: $newResult")
    }

    private fun processNotification(ciNotification: CINotification) {
        try {
            val queue = this.queueById(ciNotification.queueId)
            if(queue == null) {
                log.error("Fatal error: can't locate queue id ${ciNotification.queueId}, skipping notification")
                ciNotification.fail()
                return
            }

            val request = this.requestByQueue(queue)
            if(request == null) {
                log.error("Fatal error: can't locate request id ${queue.requestId}, skipping notification")
                ciNotification.fail()
                return
            }

            val requestObject = request.getRequest()
            val responseBuilder = ResponseBuilderFactory.createByEngine(config, requestObject.engine)
            if(responseBuilder == null) {
                log.error("Fatal error: unknown response engine: ${requestObject.engine}")
                ciNotification.fail()
                return
            }

            if(!responseBuilder.supportsResult(ciNotification.result)) {
                ciNotification.fail()
                ciNotification.sent()
                return
            }

            val job = this.jobByQueue(queue)
            if(job == null) {
                log.info("Can't locate job by queue id ($queue.id), will try again later")
                return
            }

            val buildObject = queue.getBuildObject()
            val responseJson = responseBuilder.buildResponseJSON(buildObject, job)
            if(responseJson == null) {
                log.error("Fatal error: failed to build notification response payload, skipping notification")
                ciNotification.fail()
                return
            }

            val responseUrl = responseBuilder.buildResponseURL(buildObject, job)
            if(responseUrl == null) {
                log.error("Fatal error: failed to build notification response url, skipping notification")
                ciNotification.fail()
                return
            }

            val credBuilder = config.credentialsBuilderForEngine(requestObject.engine)
            if(credBuilder == null) {
                log.error("Fatal error: no authentication configured for engine ${requestObject.engine}!")
                ciNotification.fail()
                return
            }

            val creds = credBuilder.withUrl(responseUrl).create()
            if(creds == null) {
                val urlHost = responseUrl.host
                log.error("Fatal error: missing credentials for host $urlHost, skipping notification")
                ciNotification.fail()
                return
            }

            val restClient = RestClient(creds)
            val resp = restClient.requestRawPostJSON(responseUrl.toString(), responseJson)
            if(resp.isErr()) {
                log.error("Fatal error: notification request to host service failed: ${resp.err()}")
                ciNotification.fail()
            } else {
                if(responseBuilder.validateResponse(resp.ok().body, buildObject, job)) {
                    ciNotification.sent()
                } else {
                    log.error("Fatal error: notification response object did not pass sanity check, notification POSSIBLY skipped")
                    ciNotification.fail()
                }
            }
        } finally {
            ciNotification.updateTime()
            this.updateObject(ciNotification)
        }
    }
}
