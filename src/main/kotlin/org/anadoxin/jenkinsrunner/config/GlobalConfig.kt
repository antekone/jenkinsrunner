package org.anadoxin.jenkinsrunner.config

import com.google.gson.JsonSyntaxException
import com.moandjiezana.toml.Toml
import org.anadoxin.jenkins.CredentialsBuilder
import org.anadoxin.jenkinsrunner.db.dao.RequestEngine
import java.io.*

class GlobalConfig(
    val root: RootConfig
) {
    data class ConfigJenkins(
        val url: String,
        val jobUrl: String,
        val jobDescription: String,
        val jobName: String
    )

    data class ConfigProject(
        val sourceName: String,
        val targetName: String,
        val targetArgs: Map<String, String>,
        val token: String?
    )

    data class AuthConfig(
        val type: String?,
        val basicSource: String?,
        val bearerSource: String?
    )

    data class RootConfig(
        val jenkins: ConfigJenkins,
        val projects: Map<String, ConfigProject>,
        val auth: Map<RequestEngine, AuthConfig>
    )

    val jenkinsURL: String
        get() = this.root.jenkins.url

    val projects: Map<String, ConfigProject>
        get() = this.root.projects

    fun getConfigProject(fullName: String): ConfigProject? {
        return projects.filter { (k, v) -> v.sourceName == fullName }.map { (k, v) -> v }.firstOrNull()
    }

    private fun authConfigType(type: String): CredentialsBuilder.AuthConfigType = when(type.toLowerCase()) {
        "basic" -> CredentialsBuilder.AuthConfigType.Basic
        "bearer" -> CredentialsBuilder.AuthConfigType.Bearer
        else -> CredentialsBuilder.AuthConfigType.None
    }

    fun credentialsBuilderForEngine(engine: RequestEngine): CredentialsBuilder? {
        val authConfig = this.root.auth[engine] ?: return null
        var builder = CredentialsBuilder.empty().withType(authConfigType(authConfig.type ?: ""))

        if(authConfig.basicSource != null) {
            builder = builder.withBasicSource(authConfig.basicSource)
        } else if(authConfig.bearerSource != null) {
            builder = builder.withBearerSource(authConfig.bearerSource)
        }

        return builder
    }

    companion object {
        fun defaultFileName(): String = "JenkinsRunner.toml"

        fun fromInputStream(input: InputStream): GlobalConfig? {
            try {
                val reader = BufferedReader(InputStreamReader(input))
                return fromContents(reader.readText())
            } catch(e: IOException) {
                throw RuntimeException("I/O error when trying to read the configuration. More info: $e")
            }
        }

        fun fromContents(contents: String): GlobalConfig? {
            try {
                return GlobalConfig(root = Toml().read(contents).to(RootConfig::class.java))
            } catch(e: IOException) {
                throw RuntimeException("I/O error during configuration load. More info: $e")
            } catch(e: IllegalStateException) {
                throw RuntimeException("Syntax error in TOML file. More info: $e")
            } catch(e: JsonSyntaxException) {
                throw RuntimeException("Syntax error in TOML file. More info: $e")
            }
        }
    }
}
