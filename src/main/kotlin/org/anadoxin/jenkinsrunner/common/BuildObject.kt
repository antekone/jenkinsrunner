package org.anadoxin.jenkinsrunner.common

class BuildObjects(private val list: List<BuildObject>) {
    fun isEmpty(): Boolean = list.isEmpty()
    fun isNotEmpty(): Boolean = list.isNotEmpty()

    override fun toString(): String {
        return "BuildObjects[" + list.joinToString(separator = ", ") { item ->
            "${item.fullProjectName}/${item.gitHash}"
        } + "]"
    }

    fun forEach(callback: (buildObject: BuildObject) -> (Unit)) = list.forEach(callback)
}

class BuildObject(
    val fullProjectName: String,
    val ownerId: String,
    val repoId: String,
    val gitHash: String,
    val refId: String
)
