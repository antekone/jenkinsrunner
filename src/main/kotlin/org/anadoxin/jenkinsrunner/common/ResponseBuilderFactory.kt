package org.anadoxin.jenkinsrunner.common

import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.anadoxin.jenkinsrunner.processors.bitbucketcloud.BitBucketCloudBuildResponseBuilder
import org.anadoxin.jenkinsrunner.db.dao.CIJob
import org.anadoxin.jenkinsrunner.db.dao.CIJobResult
import org.anadoxin.jenkinsrunner.db.dao.RequestEngine
import org.anadoxin.jenkinsrunner.processors.github.GitHubBuildResponseBuilder
import java.net.URL

interface BuildResponseBuilder {
    fun buildResponseJSON(buildObject: BuildObject, job: CIJob): String?
    fun buildResponseURL(buildObject: BuildObject, job: CIJob): URL?
    fun validateResponse(responseJson: String, buildObject: BuildObject, job: CIJob): Boolean
    fun supportsResult(result: CIJobResult): Boolean
}

// TODO: convert this from the Factory pattern to Dependency Injection pattern plus @attributes, like i.e. DatabaseTable in ORMlite, but with autodiscovery
class ResponseBuilderFactory {
    companion object {
        fun createByEngine(config: GlobalConfig, engine: RequestEngine): BuildResponseBuilder? {
            return when(engine) {
                RequestEngine.BitBucketCloud -> BitBucketCloudBuildResponseBuilder(config)
                RequestEngine.GitHub -> GitHubBuildResponseBuilder(config)
                else -> null
            }
        }
    }
}
