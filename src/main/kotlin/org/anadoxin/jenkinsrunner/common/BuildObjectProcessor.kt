package org.anadoxin.jenkinsrunner.common

import org.anadoxin.jenkinsrunner.db.dao.RequestEngine
import org.anadoxin.types.Result

interface BuildObjectProcessor {
    fun extract(): Result<BuildObjects, String>
    fun getEngine(): RequestEngine
}
