package org.anadoxin.jenkinsrunner

import org.anadoxin.jenkinsrunner.db.Database
import org.anadoxin.jenkinsrunner.state.StateMonitor
import org.slf4j.LoggerFactory
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MainContext

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger("main")
    logger.info("Starting up...")

    // Preload singletons
    Database.instance()
    StateMonitor.instance()

    logger.info("Proceeding with Spring Boot initialization...")
	runApplication<MainContext>(*args) {
		setBannerMode(Banner.Mode.OFF)
	}
}
