package org.anadoxin.jenkinsrunner

import com.google.gson.Gson
import com.google.gson.JsonParseException
import org.anadoxin.jenkins.BasicJenkinsClient
import org.anadoxin.jenkinsrunner.common.BuildObjectProcessor
import org.anadoxin.jenkinsrunner.processors.bitbucketcloud.BitBucketCloudProcessor
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.anadoxin.jenkinsrunner.db.Database
import org.anadoxin.jenkinsrunner.db.dao.CIRequest
import org.anadoxin.jenkinsrunner.db.dao.RequestEngine
import org.anadoxin.jenkinsrunner.processors.github.GitHubProcessor
import org.anadoxin.jenkinsrunner.request.BitBucketCloudRequest
import org.anadoxin.jenkinsrunner.request.GitHubRequest
import org.anadoxin.jenkinsrunner.response.ErrorResponse
import org.anadoxin.jenkinsrunner.response.OKResponse
import org.anadoxin.jenkinsrunner.response.ProjectInfoResponse
import org.anadoxin.jenkinsrunner.response.ProjectListResponse
import org.anadoxin.jenkinsrunner.state.StateMonitor
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.File
import java.io.FileInputStream
import java.net.URL

@RestController
@EnableAutoConfiguration
object MainController {
    private val client: BasicJenkinsClient
    private val logger = LoggerFactory.getLogger(this::class.java)

    private fun checkConfig(config: GlobalConfig): Boolean {
        InitializationChecks.checkProjectNameConsistency(client, config) || return false

        return true
    }

    private fun loadConfig(): GlobalConfig? {
        return this.loadConfig(System.getenv("PWD"))
    }

    private fun loadConfig(currentDir: String): GlobalConfig? {
        val configPath = File(currentDir, GlobalConfig.defaultFileName())
        return GlobalConfig.fromInputStream(FileInputStream(configPath))
    }

    init {
        logger.info("Performing JenkinsRunner sanity checks on startup...")
        val config = loadConfig() ?: throw RuntimeException("Can't load configuration file")

        val url = URL(config.jenkinsURL)
        val credBuilder = config.credentialsBuilderForEngine(RequestEngine.Jenkins) ?:
            throw RuntimeException("Invalid credential setup for Jenkins")

        val creds = credBuilder.withUrl(url).create() ?:
            throw RuntimeException("Invalid credentials for Jenkins")

        this.client = BasicJenkinsClient(url, creds)
        if(!checkConfig(config)) {
            throw RuntimeException("Project configuration is invalid")
        }

        StateMonitor.instance().setJenkinsClient(this.client)
        StateMonitor.instance().setGlobalConfig(config)

        logger.info("Main controller initialization finished")
    }

    @RequestMapping("/clearCache")
    fun clearCache(): ResponseEntity<String> {
        // TODO: implement this
        return OKResponse.respond("needs to be implemented")
    }

    @RequestMapping("/projectList")
    fun projectList(): ResponseEntity<String> {
        val config = loadConfig() ?: return ErrorResponse.respond(1, "Internal Server Error", "Configuration load error")
        val lst = config.projects.map { (_, v) -> v.targetName }.toList()
        return ProjectListResponse.respond(lst)
    }

    @RequestMapping("/projectInfo/byTargetName/{targetName}")
    fun projectInfoByTargetName(@PathVariable("targetName") targetName: String): ResponseEntity<String> {
        val config = loadConfig() ?: return ErrorResponse.respond(1, "Internal Server Error", "Configuration load error")
        val lst = config.projects.filter { (_, v) -> v.targetName == targetName }.map { (_, v) -> v }.toList()
        if(lst.isNullOrEmpty()) {
            return ErrorResponse.respond(2, "Not found", "No project by that target name: $targetName")
        }

        return ProjectInfoResponse.respond(lst[0].sourceName, lst[0].targetName)
    }

    private fun createRequest(req: CIRequest): Boolean {
        val dao = Database().createDao<CIRequest>()
        return dao.createOrUpdate(req).numLinesChanged > 0
    }

    private inline fun <reified RequestClass>
        queueRequest(jsonText: String, processorBuilder: (RequestClass) -> (BuildObjectProcessor)): ResponseEntity<String>
    {
        return try {
            val jsReq = Gson().fromJson(jsonText, RequestClass::class.java) ?:
                return ErrorResponse.respond(2, "Invalid request", "Your request JSON was invalid.")

            val proc = processorBuilder(jsReq)
            val builds = proc.extract()
            if(builds.isErr()) {
                return ErrorResponse.respond(2, "Invalid request", "Your request did not contain any build objects, " +
                    "or was invalid. More info: ${builds.err()}")
            }

            val req = CIRequest.create(proc.getEngine(), jsonText)
            return if(!this.createRequest(req)) {
                OKResponse.respond("Request error: nothing was added")
            } else {
                OKResponse.respond("Request queued OK")
            }
        } catch(e: JsonParseException) {
            ErrorResponse.respond(1, "Bad JSON", "Invalid JSON for GitHub endpoint")
        }
    }

    @PostMapping("/trigger/byServiceName/GitHub")
    fun triggerGitHub(@RequestBody req: String): ResponseEntity<String> =
        queueRequest<GitHubRequest>(req) { r ->
            GitHubProcessor(r)
        }

    @PostMapping("/trigger/byServiceName/BitBucketCloud")
    fun triggerBitBucket(@RequestBody req: String): ResponseEntity<String> =
        queueRequest<BitBucketCloudRequest>(req) { r ->
            BitBucketCloudProcessor(r)
        }
}

