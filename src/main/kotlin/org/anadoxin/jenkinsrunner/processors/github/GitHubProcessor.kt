package org.anadoxin.jenkinsrunner.processors.github

import org.anadoxin.jenkinsrunner.common.BuildObject
import org.anadoxin.jenkinsrunner.common.BuildObjectProcessor
import org.anadoxin.jenkinsrunner.common.BuildObjects
import org.anadoxin.jenkinsrunner.db.dao.RequestEngine
import org.anadoxin.jenkinsrunner.request.GitHubRequest
import org.anadoxin.types.Result

class GitHubProcessor(val req: GitHubRequest) : BuildObjectProcessor {
    override fun getEngine(): RequestEngine = RequestEngine.GitHub

    override fun extract(): Result<BuildObjects, String> {
        val buildObject = BuildObject(
            fullProjectName = req.repository.full_name,
            gitHash = req.after,
            ownerId = req.repository.owner.name,
            repoId = req.repository.name,
            refId = req.ref.replaceFirst("refs/heads/", "")
        )

        val wrapper = BuildObjects(listOf(buildObject))
        return Result.ok(wrapper)
    }
}
