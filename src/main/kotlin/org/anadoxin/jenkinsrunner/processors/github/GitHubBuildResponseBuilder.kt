package org.anadoxin.jenkinsrunner.processors.github

import com.google.gson.Gson
import com.google.gson.stream.MalformedJsonException
import io.mikael.urlbuilder.UrlBuilder
import org.anadoxin.jenkinsrunner.common.BuildObject
import org.anadoxin.jenkinsrunner.common.BuildResponseBuilder
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.anadoxin.jenkinsrunner.db.dao.CIJob
import org.anadoxin.jenkinsrunner.db.dao.CIJobResult
import java.net.MalformedURLException
import java.net.URL

class GitHubBuildResponseBuilder(private val config: GlobalConfig) : BuildResponseBuilder {
    override fun buildResponseURL(buildObject: BuildObject, job: CIJob): URL? {
        return try {
            UrlBuilder
                .fromString("https://api.github.com/repos/${buildObject.ownerId}/${buildObject.repoId}/statuses/${buildObject.gitHash}")
                .toUrl()
        } catch(e: MalformedURLException) {
            null
        }
    }

    data class Response(
        val url: String,
        val state: String,
        val node_id: String
    )

    override fun validateResponse(responseJson: String, buildObject: BuildObject, job: CIJob): Boolean {
        try {
            val resp = Gson().fromJson(responseJson, Response::class.java) ?: return false
            return resp.state == jobResultToGitHubState(job.result)
        } catch(e: MalformedJsonException) {
            return false
        }
    }

    override fun supportsResult(result: CIJobResult): Boolean {
        return result == CIJobResult.Started || result == CIJobResult.Failure || result == CIJobResult.Success
    }

    data class Request(
        val state: String,
        val target_url: String,
        val description: String,
        val context: String)

    private fun jobResultToGitHubState(r: CIJobResult): String = when(r) {
        CIJobResult.Started -> "pending"
        CIJobResult.Success -> "success"
        CIJobResult.Failure -> "failure"
        else -> "error"
    }

    override fun buildResponseJSON(buildObject: BuildObject, job: CIJob): String? {
        val resp = Request(
            state = jobResultToGitHubState(job.result),
            context = config.root.jenkins.jobName,
            description = config.root.jenkins.jobDescription,
            target_url = job.url)

        return Gson().toJson(resp)
    }
}
