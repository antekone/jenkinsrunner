package org.anadoxin.jenkinsrunner.processors.bitbucketcloud

import org.anadoxin.jenkinsrunner.common.BuildObject
import org.anadoxin.jenkinsrunner.common.BuildObjectProcessor
import org.anadoxin.jenkinsrunner.common.BuildObjects
import org.anadoxin.jenkinsrunner.db.dao.RequestEngine
import org.anadoxin.jenkinsrunner.request.BitBucketCloudRequest
import org.anadoxin.types.Result
import org.slf4j.LoggerFactory

class BitBucketCloudProcessor(private val req: BitBucketCloudRequest) : BuildObjectProcessor {
    override fun getEngine(): RequestEngine = RequestEngine.BitBucketCloud

    private val log = LoggerFactory.getLogger(BitBucketCloudProcessor::class.java)
    override fun extract(): Result<BuildObjects, String> {
        if(req.push == null)
            return Result.err("Invalid request structure")

        class Hash(val hash: String, val refId: String)

        val hashList: List<Hash> = req.push.changes.map { change ->
            if(change.new != null) {
                if(change.new.type != "branch") {
                    log.info("Change NEW type is not a branch: ${change.new.type}")
                    return@map null
                }
            } else {
                log.info("Change NEW is null; branch has been deleted?")
                return@map null
            }

            return@map change.commits.map { commit ->
                Hash(commit.hash, change.new.name)
            }
        }.filter { m -> m != null }.flatMap { m -> m!! }

        val objects = BuildObjects(hashList.map { h ->
            BuildObject(
                gitHash = h.hash,
                fullProjectName = req.repository.full_name,
                refId = h.refId,
                ownerId = req.repository.owner.uuid,
                repoId = req.repository.uuid
            )
        }.take(1))

        return if(objects.isEmpty()) {
            Result.err("No build objects found")
        } else {
            Result.ok(objects)
        }
    }
}
