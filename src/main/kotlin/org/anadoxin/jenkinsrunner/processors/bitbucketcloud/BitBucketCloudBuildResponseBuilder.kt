package org.anadoxin.jenkinsrunner.processors.bitbucketcloud

import com.google.gson.Gson
import com.google.gson.stream.MalformedJsonException
import io.mikael.urlbuilder.UrlBuilder
import org.anadoxin.jenkinsrunner.common.BuildObject
import org.anadoxin.jenkinsrunner.common.BuildResponseBuilder
import org.anadoxin.jenkinsrunner.config.GlobalConfig
import org.anadoxin.jenkinsrunner.db.dao.CIJob
import org.anadoxin.jenkinsrunner.db.dao.CIJobResult
import org.slf4j.LoggerFactory
import java.net.URL

class BitBucketCloudBuildResponseBuilder(private val config: GlobalConfig) : BuildResponseBuilder {
    private val log = LoggerFactory.getLogger(BitBucketCloudBuildResponseBuilder::class.java)!!

    override fun buildResponseURL(buildObject: BuildObject, job: CIJob): URL? {
        return UrlBuilder.fromString("https://api.bitbucket.org")
            .withPath("/2.0/repositories/" +
                "${buildObject.ownerId}/${buildObject.repoId}/commit/${buildObject.gitHash}/statuses/build")
            .toUrl()
    }

    class JSONData(
        val key: String,
        val state: String,
        val name: String,
        val url: String,
        val description: String
    )

    class Response(
        val type: String,
        val key: String,
        val name: String)

    fun jobResultToString(result: CIJobResult): String? = when(result) {
        CIJobResult.Success -> "SUCCESSFUL"
        CIJobResult.Failure -> "FAILED"
        CIJobResult.Started -> "INPROGRESS"
        else -> null
    }

    override fun buildResponseJSON(buildObject: BuildObject, job: CIJob): String? {
        val stateString = jobResultToString(job.result)
        if(stateString == null) {
            log.error("Error: this response builder can't handle result status ${job.result}!")
            return null
        }

        val data = JSONData(
            key = buildObject.gitHash,
            state = stateString,
            url = job.url,
            name = "JenkinsRunner",
            description = "Sample job description")

        return Gson().toJson(data)
    }

    override fun validateResponse(responseJson: String, buildObject: BuildObject, job: CIJob): Boolean {
        try {
            val resp = Gson().fromJson(responseJson, Response::class.java) ?: return false
            return resp.key == buildObject.gitHash && resp.type.toLowerCase() == "build"
        } catch(e: MalformedJsonException) {
            return false
        }
    }

    override fun supportsResult(result: CIJobResult): Boolean {
        return result == CIJobResult.Started || result == CIJobResult.Success || result == CIJobResult.Failure
    }
}
