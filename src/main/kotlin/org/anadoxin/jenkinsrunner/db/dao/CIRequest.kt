package org.anadoxin.jenkinsrunner.db.dao

import com.google.gson.Gson
import com.j256.ormlite.field.DataType
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.whoischarles.util.json.Minify
import org.joda.time.DateTime
import java.io.*
import java.util.*
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

enum class RequestEngine {
    Unspecified, Jenkins, BitBucketCloud, GitHub
}

@DatabaseTable(tableName = CIRequest.TABLE)
class CIRequest private constructor() {
    class Request(
        val engine: RequestEngine = RequestEngine.Unspecified,
        val body: String? = null
    )

    companion object {
        const val TABLE = "cirequests"
        const val CREATED = "created"
        const val UPDATED = "updated"
        const val HANDLED = "handled"
        const val ID = "id"

        fun create(engine: RequestEngine, reqString: String): CIRequest {
            val req = CIRequest()
            req.created = DateTime.now()
            req.updated = DateTime.now()
            req.handled = false
            req.setRequest(Request(
                engine = engine,
                body = reqString))
            return req
        }
    }

    fun updateTime() {
        this.updated = DateTime.now()
    }

    @DatabaseField(columnName = ID, generatedId = true)
    val id: Int = 0

    @DatabaseField(columnName = CIRequest.CREATED, index = true)
    lateinit var created: DateTime

    @DatabaseField(columnName = CIRequest.UPDATED, index = true)
    lateinit var updated: DateTime

    @DatabaseField
    var failed: Boolean = false

    @DatabaseField(columnName = CIRequest.HANDLED, index = true)
    var handled: Boolean = false

    @DatabaseField(dataType = DataType.LONG_STRING)
    lateinit var compressedRequest: String

    fun getRequest(): Request {
        val string = BufferedReader(InputStreamReader(GZIPInputStream(ByteArrayInputStream(
            Base64.getDecoder().decode(compressedRequest))))).use {
                it.readText()
        }

        return Gson().fromJson(string, Request::class.java)
    }

    fun setRequest(r: Request) {
        val string = Minify().minify(Gson().toJson(r))
        val buffer = ByteArrayOutputStream()

        BufferedWriter(OutputStreamWriter(GZIPOutputStream(buffer))).use {
            it.write(string)
        }

        val byteArray = buffer.toByteArray()
        compressedRequest = Base64.getEncoder().encodeToString(byteArray)
    }
}
