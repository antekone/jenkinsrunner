package org.anadoxin.jenkinsrunner.db.dao

import com.google.gson.Gson
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.anadoxin.jenkinsrunner.common.BuildObject
import org.joda.time.DateTime

@DatabaseTable(tableName = CIQueue.TABLE)
class CIQueue {
    companion object {
        const val TABLE = "ciqueues"
        const val QUEUE_ID = "id"
        const val REQUEST_ID = "requestid"
        const val REMOTE_QUEUE_ID = "remoteid"
        const val CREATED = "created"
        const val UPDATED = "updated"
        const val HANDLED = "handled"
        const val REQUEST_URL = "requesturl"
        const val BUILD_OBJECT = "buildobject"

        fun create(remoteId: Int, requestId: Int, buildObject: BuildObject, url: String): CIQueue {
            val q = CIQueue()
            q.requestURL = url
            q.created = DateTime.now()
            q.updated = DateTime.now()
            q.handled = false
            q.remoteId = remoteId
            q.requestId = requestId
            q.setBuildObject(buildObject)
            return q
        }
    }

    fun updateTime() {
        this.updated = DateTime.now()
    }

    fun getBuildObject(): BuildObject = Gson().fromJson(this.buildObject, BuildObject::class.java)
    fun setBuildObject(bo: BuildObject) {
        this.buildObject = Gson().toJson(bo)
    }

    @DatabaseField(columnName = CIQueue.QUEUE_ID, generatedId = true)
    val id: Int = 0

    @DatabaseField(columnName = CIQueue.REMOTE_QUEUE_ID)
    var remoteId: Int = 0

    @DatabaseField(columnName = CIQueue.REQUEST_ID)
    var requestId: Int = 0

    @DatabaseField(columnName = BUILD_OBJECT)
    private lateinit var buildObject: String

    @DatabaseField(columnName = CIQueue.CREATED, index = true)
    lateinit var created: DateTime

    @DatabaseField(columnName = CIQueue.UPDATED, index = true)
    lateinit var updated: DateTime

    @DatabaseField(columnName = CIQueue.HANDLED, index = true)
    var handled: Boolean = false

    @DatabaseField(columnName = CIQueue.REQUEST_URL)
    lateinit var requestURL: String
}
