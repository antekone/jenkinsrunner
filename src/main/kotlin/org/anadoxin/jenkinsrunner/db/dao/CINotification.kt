package org.anadoxin.jenkinsrunner.db.dao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.joda.time.DateTime

@DatabaseTable(tableName = CINotification.TABLE)
class CINotification private constructor() {
    companion object {
        const val TABLE = "cinotifications"
        const val ID = "id"
        const val QUEUE_ID = "queueid"
        const val RESULT = "newresult"
        const val SENT = "sent"
        const val FAILED = "failed"
        const val NOT_SUPPORTED = "notsupported"
        const val CREATED = "created"
        const val UPDATED = "updated"

        fun create(queueId: Int, result: CIJobResult): CINotification {
            val n = CINotification()
            n.queueId = queueId
            n.result = result
            n.sent = false
            n.created = DateTime.now()
            n.updateTime()
            return n
        }
    }

    fun updateTime() {
        this.updated = DateTime.now()
    }

    fun fail() {
        this.failed = true
    }

    fun sent() {
        this.sent = true
    }

    @DatabaseField(columnName = ID, generatedId = true)
    val id: Int = 0

    @DatabaseField(columnName = QUEUE_ID, index = true)
    var queueId: Int = 0

    @DatabaseField(columnName = SENT, index = true)
    var sent: Boolean = false

    @DatabaseField(columnName = FAILED, index = true)
    var failed: Boolean = false

    @DatabaseField(columnName = RESULT)
    lateinit var result: CIJobResult

    @DatabaseField(columnName = CREATED)
    lateinit var created: DateTime

    @DatabaseField(columnName = UPDATED)
    lateinit var updated: DateTime

}
