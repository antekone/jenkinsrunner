package org.anadoxin.jenkinsrunner.db.dao

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import org.joda.time.DateTime

enum class CIJobResult {
    NotExists, Created, Started, Success, Failure
}

@DatabaseTable(tableName = CIJob.TABLE)
class CIJob {
    companion object {
        const val TABLE = "cijobs"
        const val RESULT = "result"
        const val CREATED = "created"
        const val QUEUE_ID = "queueid"
        const val UPDATED = "updated"
        const val PROJECT_NAME = "projectname"

        fun create(queueId: Int, projectName: String, url: String): CIJob {
            val job = CIJob()
            job.result = CIJobResult.Created
            job.created = DateTime.now()
            job.queueId = queueId
            job.url = url
            job.projectName = projectName
            job.updateTime()
            return job
        }
    }

    @DatabaseField(generatedId = true)
    val id: Int = 0

    @DatabaseField(columnName = CIJob.QUEUE_ID, index = true)
    var queueId: Int = 0

    @DatabaseField
    lateinit var url: String

    @DatabaseField(columnName = CIJob.PROJECT_NAME)
    lateinit var projectName: String

    @DatabaseField(columnName = CIJob.RESULT, index = true)
    lateinit var result: CIJobResult

    @DatabaseField(columnName = CIJob.CREATED, index = true)
    lateinit var created: DateTime

    @DatabaseField(columnName = CIJob.UPDATED, index = true)
    lateinit var updated: DateTime

    fun updateTime() {
        updated = DateTime.now()
    }
}
