package org.anadoxin.jenkinsrunner.db

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.jdbc.JdbcConnectionSource
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import org.anadoxin.jenkinsrunner.db.dao.CIJob
import org.anadoxin.jenkinsrunner.db.dao.CINotification
import org.anadoxin.jenkinsrunner.db.dao.CIQueue
import org.anadoxin.jenkinsrunner.db.dao.CIRequest
import org.slf4j.LoggerFactory

abstract class AbstractDatabase {
    abstract fun source(): ConnectionSource

    abstract fun <T> createDao(c: Class<T>): Dao<T, out Any>

    inline fun <reified O> createDao(): Dao<O, out Any> {
        return this.createDao(O::class.java)
    }
}

enum class DBType {
    Production, Test
}

class Database private constructor() {
    companion object {
        var singleton: AbstractDatabase? = null

        operator fun invoke(): AbstractDatabase = Database.instance()

        fun instance(dbType: DBType = DBType.Production): AbstractDatabase {
            when(dbType) {
                DBType.Production -> {
                    if(singleton == null)
                        singleton = ProductionH2Database()

                    return singleton!!
                }

                DBType.Test -> {
                    throw NotImplementedError("not implemented yet")
                }
            }
        }
    }
}

class ProductionH2Database : AbstractDatabase() {
    private val cs: ConnectionSource
    private val logger = LoggerFactory.getLogger(ProductionH2Database::class.java)

    init {
        val pwd = System.getenv("PWD")
        if(pwd.contains(":"))
            throw RuntimeException("Sorry, you can't use ':' character as a current directory name!")

        val dbString = "jdbc:h2:$pwd/runnerdata;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9999"
        logger.info("Creating database: $dbString")

        this.cs = JdbcConnectionSource(dbString)
        TableUtils.createTableIfNotExists(this.cs, CIJob::class.java)
        TableUtils.createTableIfNotExists(this.cs, CIRequest::class.java)
        TableUtils.createTableIfNotExists(this.cs, CIQueue::class.java)
        TableUtils.createTableIfNotExists(this.cs, CINotification::class.java)

        logger.info("Database object created.")
    }

    override fun source(): ConnectionSource {
        return this.cs
    }

    override fun <T> createDao(c: Class<T>): Dao<T, out Any> {
        return DaoManager.createDao(this.cs, c)
    }
}

