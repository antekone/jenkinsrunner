=====
About
=====

This is a JenkinsRunner project. A small (if a JVM-based server can be small) microservice that listens for web hook events from sites like GitHub, BitBucket (Cloud), Stash (BitBucket local instance), and possibly other sites (maybe SourceHut). When it gets an event, it will issue a Jenkins build request to a configured Jenkins node (that is under your control).

After Jenkins build is complete, JenkinsRunner will issue a callback request to GitHub, BitBucket, etc, updating the build status of the project.

So, with JenkinsRunner, you can create yourself a functional continuous integration pipeline.

Current version of JenkinsRunner is in development stage, it means it's not functional yet, so don't even try to use it yet. I'm working on it!

How to use it?
^^^^^^^^^^^^^^

In order to use it, all you need to do is the following:

1. Checkout the sources of JenkinsRunner, or download an already-compled JAR file from somewhere (doesn't exist yet in this repository),

2. Run ``./gradlew assemble`` to generate your own ``jar`` file,

3. Create your configuration file, ``JenkinsRunner.toml``, in your current work dir (use ``echo $PWD`` to know your current work dir ;)). Use ``JenkinsRunner.toml.example`` file to see the example configuration. Modify the example to fit your use-case.

4. Run the server: ``java -jar ./build/libs/jenkinsrunner-*.jar``.

5. You can copy around the JAR file to other servers/operating systems. It should be just this one JAR file and the TOML configuration file, nothing else should be needed.

FAQ
^^^

| Q: What the heck is TOML?
| A: TOML is a configuration language, used widely in Rust programming language environment, but not only there. And since I like Rust, I've decided to use TOML. Don't worry, it's like INI, but better.
|
| Q: If you like Rust, why the server is running on a JVM?
| A: Because now you can use the same JAR file to run the server on multiple architectures. Also you can develop on Linux and run the binary on FreeBSD, etc.
|
| Q: Java is slow.
| A: It's not 1998 anymore. Check benchmarks.
|
| Q: Why not Scala?
| A: Why not Kotlin?
|
| Q: Why not Python?
| A: It's not my type!

Author
^^^^^^

This piece of software was written by Grzegorz Antoniak for YOUR convinience! Isn't that nice? You can contact me on
Mastodon: `<https://mstdn.io/@antekone>`_.

=======
License
=======

::

    JenkinsRunner, the tool to trigger Jenkins build events based on WebHooks,
    Copyright (C) 2010-2100 Grzegorz Antoniak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Yes, it's GPL 3.0.
