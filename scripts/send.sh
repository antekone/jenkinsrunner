#!/usr/bin/env sh

FILE="$2"
curl -H "Content-type: application/json" -d @$FILE http://localhost:8080/$1
